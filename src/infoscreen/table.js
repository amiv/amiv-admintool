import m from 'mithril';
import { Snackbar } from 'polythene-mithril';
import { apiUrl } from 'networkConfig';
import { DatalistController } from 'amiv-web-ui-components';
import { ResourceHandler } from '../auth';
import { dateFormatter } from '../utils';
import TableView from '../views/tableView';

const getImgUrl = img => `${apiUrl}${img.file}`;

const exportCSV = (ctrl) => {
  ctrl.getFullList().then(
    (list) => {
      let csv = '';
      csv += [
        'id',
        'title_en',
        'time_advertising_start',
        'time_advertising_end',
        'img_infoscreen_url',
      ].join(';');
      csv += '\n';
      list.forEach((event) => {
        const fields = [
          event._id,
          event.title_en.replace(';', ''),
          event.time_advertising_start,
          event.time_advertising_end,
          event.img_infoscreen ? getImgUrl(event.img_infoscreen) : '',
        ];
        csv += fields.join(';');
        csv += '\n';
      });
      const blob = new Blob([csv], { type: 'text/csv' });
      const dl = window.document.createElement('a');
      const now = new Date();
      const pain = [
        now.getFullYear().toString(),
        now.getMonth().toString().padStart(2, '0'),
        now.getDay().toString().padStart(2, '0')].join('-');
      dl.href = window.URL.createObjectURL(blob);
      dl.download = `infoscreen-export_${pain}.csv`;
      dl.style.display = 'none';
      document.body.appendChild(dl);
      dl.click();
      document.body.removeChild(dl);
    },
    () => {
      Snackbar.show({
        title: 'Export failed',
        style: { color: 'red' },
      });
    },
  );
};


export default class InfoscreenTable {
  constructor() {
    this.handler = new ResourceHandler('events');
    this.ctrl = new DatalistController((query, search) => this.handler.get({ search, ...query }));
  }


  getItemData(data) {
    return [
      m('div', { style: { width: 'calc(100% - 36em)' } }, data.title_de || data.title_en),
      m('div', { style: { width: '9em' } }, dateFormatter(data.time_start)),
      m('div', { style: { width: '9em' } }, dateFormatter(data.time_advertising_start)),
      m('div', { style: { width: '9em' } }, dateFormatter(data.time_advertising_end)),
      m('div',
        { style: { width: '9em' } },
        data.img_infoscreen
          ? m('a', { href: getImgUrl(data.img_infoscreen) }, data.img_infoscreen.name)
          : 'no image'),
    ];
  }

  view() {
    const now = new Date();
    return m(TableView, {
      controller: this.ctrl,
      keys: [
        'titel_en',
        'time_start',
        'time_advertising_start',
        'time_advertising_start',
        'img_infoscreen'],
      tileContent: this.getItemData,
      titles: [
        { text: 'Title', width: 'calc(100% - 36em)' },
        { text: 'Start', width: '9em' },
        { text: 'Advertising Start', width: '9em' },
        { text: 'Advertising End', width: '9em' },
        { text: 'Infoscreen Image', width: '9em' },
      ],
      filters: [[{
        name: 'upcoming',
        query: { time_start: { $gte: `${now.toISOString().slice(0, -5)}Z` } },
      },
      {
        name: 'advertising upcoming',
        query: { time_advertising_start: { $gte: `${now.toISOString().slice(0, -5)}Z` } },
      },
      {
        name: 'advertising in progress',
        query: {
          time_advertising_start: { $lte: `${now.toISOString().slice(0, -5)}Z` },
          time_advertising_end: { $gte: `${now.toISOString().slice(0, -5)}Z` },
        },
      },
      ],
      [{
        name: 'has image',
        query: { img_infoscreen: { $ne: null } },
      }]],
      buttons: [
        { text: 'Export CSV', onclick: () => exportCSV(this.ctrl) },
      ],
      // per default, enable the 'upcoming' filter
      initFilterIdxs: [[0, 0], [1, 0]],
    });
  }
}
